package com.example.android.receitas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ImageView mImageView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private ArrayList<String> receitas;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        receitas = new ArrayList<>();
        for(int i=1; i < 13;i++) {
            receitas.add("bacalhau" + i );
        }


        mRecyclerView = (RecyclerView) findViewById(R.id.rv_receitas);
        mImageView = (ImageView) findViewById(R.id.iv_receita);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ReceitasAdapter(receitas, mImageView, this);
        mRecyclerView.setAdapter(mAdapter);


    }
}