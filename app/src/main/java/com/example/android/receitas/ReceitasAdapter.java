package com.example.android.receitas;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

/**
 * Created by asousa on 17-02-2018.
 */

public class ReceitasAdapter extends RecyclerView.Adapter<ReceitasAdapter.ReceitaHolder> {

    private ArrayList<String> mReceitas;
    private ImageView mIvImagem;
    private Context mContext;
    private Toast mToast;

    public ReceitasAdapter(ArrayList<String> receitas, ImageView iv, Context context) {
        mReceitas = receitas;
        mIvImagem = iv;
        mContext = context;
    }

    @Override
    public ReceitaHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);

        ReceitaHolder vh = new ReceitaHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReceitaHolder holder, int position) {
        holder.nomeReceita.setText(mReceitas.get(position));
    }

    @Override
    public int getItemCount() {
        return mReceitas.size();
    }


    /*
    --------- VIEW HOLDER -----------
     */
    public class ReceitaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nomeReceita;

        public ReceitaHolder(View itemView) {
            super(itemView);
            nomeReceita = itemView.findViewById(R.id.tv_nome_receita);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String nomeReceita = mReceitas.get(getAdapterPosition());
            mIvImagem.setImageResource(mContext.getResources().getIdentifier(nomeReceita,"drawable", mContext.getPackageName()));
            if( mToast != null ) {
                mToast.cancel();
            }
            mToast = Toast.makeText(mContext,"Receita clicada: " + nomeReceita, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    /*
    ----------------------------------
     */


}
